<?php

/*
public_html/projets/PawPaul
*/

namespace App\Controller;

use App\Entity\Question;
use App\Entity\Answer;
use App\Form\QuestionType;
use App\Form\AnswerType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class MainController extends AbstractController
{

    // ==================== C R U D ==================== //
    /**
     * @Route("/", name="main")
     */
    public function home()
    {
        return $this->render('pages/home.html.twig', [
            'controller_name' => 'MainController',
        ]);
    }

    /**
     * @Route("/create-pawpaul", name ="createPoll")
     */
    public function createPoll(Request $request)
    {
        // form creation
        $poll = new Question();
        $form = $this->createForm(QuestionType::class, $poll);

        // form handle
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid())
        {
            $em = $this->getDoctrine()->getManager();
            $em->persist($poll);
            $em->flush();

            $id = $poll->getId();

            dd($request);

            $this->addFlash('success', 'Pawpaul created! The world is yours. ;)');

            // Vue form (created)
            return $this->redirectToRoute("pollById", [
                "id" => $id
            ]);
        }
        
        // Vue form (empty)
        return $this->render("pages/create_poll.html.twig", [
            "form" => $form->createView()
        ]);
    }



    /**
     * @Route("see-all-pawpauls", name="renderAllPolls")
     */
    public function renderPolls()
    {
        $repo = $this->getDoctrine()->getRepository(Question::class);
        $polls = $repo->findAll();

        return $this->render("pages/all_polls.html.twig", [
            "polls" => $polls
        ]);
    }

    /**
     * @Route("update-pawpaul/{id}", name="updatePoll")
     */
    public function updatePoll(Request $request, $id)
    {
        // poll to update: getter
        $repo = $this->getDoctrine()->getRepository(Question::class);
        $pollToUptade = $repo->find($id);
        
        // poll to update: form creation
        $form = $this->createForm(Question::class, $pollToUptade);

        // updated poll: handle form
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $poll = $form->getData();

            $em = $this->getDoctrine()->getManager();
            $em->persist($poll);
            $em->flush();

            $id = $poll->getId;

            $this->addFlash('success', "Your Pawpaul has been updated! You're the best. ;)");
            
            return $this->redirectToRoute("pollById", [
                "id" => $id
            ]);
        } else { // in case of cancel
            return $this->render("poll_by_id.html.twig", [
                // "poll" => $poll
            ]);
        }

        return $this->render("pages/create_poll.html.twig", [
            "form" => $form
        ]);
    }

    // /**
    //  * @Route("delete-a-pawpaul", name="deletePoll")
    //  */
    // public function deletePoll()
    // {
        
    // }

    // ==================== Q U E S T I O N ==================== //

    /**
     * @Route("pawpaul/{id}", name="pollById")
     */
    public function renderPollById($id)
    {
        $repo = $this->getDoctrine()->getRepository(Question::class);
        $poll = $repo->find($id);

        return $this->render("pages/poll_by_id.html.twig", [
            "poll" => $poll
        ]);
    }

    /**
     * @Route("results/{question}", name="renderResults")
     */
    public function renderResults()
    {   
        $poll = $this->getDoctrine()->getRepository(Question::class);
        $answer = $this->getDoctrine()->getRepository(Answer::class);
        
        // return $this->render("results.html.twig", [
        //     "poll" => $poll
        // ]);
    }

    // ==================== A N S W E R ==================== //

    public function createAnswer()
    {
        $answer = new $answer;
    }

}